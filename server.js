require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const userFile = require('./user_data.json')

app.listen(port,function()
{
  console.log('NodeJS escuchando en el puesto '+port);
}
);

app.use(body_parser.json());

//Operacion GET
app.get('/holamundos',
  function(request, response){
    console.log('Hola Mundo');
    console.log('Hola Chorrillos');
    response.send('Hola Lima-Peru Latin');
  });

app.get(URL_BASE + 'users',
  function(request,response){
    response.status(222).send(userFile);//La respuesta send al final
  }
);

//Peticion GET
app.get(URL_BASE + 'users/:id_user1',
function(request,response){
  console.log(request.params.id_user1);
  let pos = request.params.id_user1 -1;
  let respuesta =
          (userFile[pos] == undefined) ? {"msg":"Usuario no Existe"} : userFile[pos];
  response.send(respuesta);
}
);

//Get con query string
app.get(URL_BASE + 'usersq',
  function(req,res){
      console.log(req.query.id);
      console.log(req.query.country);
      //res.send(userFile[req.query.id - 1]);
      let respuesta =
              (userFile[req.query.id - 1] == undefined) ? {"msg":"Usuario no Existe"} : userFile[req.query.id - 1];
      res.send(respuesta);
      res.send({"msg":"Get con Query"});
  }
);


app.get (URL_BASE + 'total_users',
  function(req,res){
    let num_usuarios = userFile.length;
    res.send({"num_usuarios" : num_usuarios});
  }
);


//Peticion POST a User
app.post(URL_BASE + 'users',
function(req,res){
  console.log('Post a Usuarios');
  let tam = userFile.length;
  let new_user = {
    "id_user":tam + 1,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
  }
  console.log(new_user);
  userFile.push(new_user);
  res.send({"msg":"Usuario creado correctamente"});
});

// Peticion PUT
app.put(URL_BASE + 'users/:id_user1',
  function(req,res){
    console.log('Entrando a PUT');
    console.log(req.params.id_user1);
    let pos = req.params.id_user1 - 1;
    let upd_user = {
      "id_user":pos,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    };
    console.log(upd_user);
    userFile[pos] = upd_user;
    res.send({"msg":"Usuario actualizado correctamente"});
  });

  app.put(URL_BASE + 'users_v2/:id',
     function(req, res){
       console.log("PUT /techu/v1/users_v2/:id");
       let idBuscar = req.params.id;
       let updateUser = req.body;
       for(i = 0; i < userFile.length; i++) {
         console.log(userFile[i].id);
         if(userFile[i].id == idBuscar) {
           userFile[i] = updateUser;
           res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
         }
       }
       res.send({"msg" : "Usuario no encontrado.", updateUser});
     });

//Deleter
app.delete(URL_BASE + 'users/:id',
       function(req,res){
         console.log("Entro a Delete");
         let idBuscar = req.params.id-1;
         for(i = 0; i < userFile.length; i++) {
           console.log(userFile[i].id_user);
           if(userFile[i].id_user == idBuscar) {
             userFile.splice(idBuscar,1);
             res.send({"msg" : "Usuario eliminado correctamente.", idBuscar});
           }
         }
         res.send({"msg":"Usuario no encontrado"});
       }
);

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    let user = request.body.email;
    let pass = request.body.password;
    for(us of userFile) {
         if(us.email == user) {
           console.log('entro for Login email');
           if(us.password == pass) {
             us.logged = true;
             writeUserDataToFile(userFile);
             console.log("Login correcto!");
             response.send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : "true"});
           } else {
             console.log("Login incorrecto.");
             response.send({"msg" : "Login incorrecto."});
           }
         }
       }}
   );

   // LOGOUT - users.json
app.post(URL_BASE + 'logout',
     function(request, response) {
       console.log("POST /apicol/v2/logout");
       var userId = request.body.id_user;
       for(us of userFile) {
         if(us.id_user == userId) {
           console.log('entro for Logout email');
           if(us.logged) {
             delete us.logged; // borramos propiedad 'logged'
            writeUserDataToFile(userFile);
            console.log("Logout correcto!");
            response.send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
          } else {
            console.log("Logout incorrecto.");
            response.send({"msg" : "Logout incorrecto."});
          }
        }  us.logged = true
      }
});


function writeUserDataToFile(data) {
     var fs = require('fs');
     var jsonUserData = JSON.stringify(data);
     fs.writeFile("./user_data.json", jsonUserData, "utf8",
      function(err) { //función manejadora para gestionar errores de escritura
        if(err) {
          console.log(err);
        } else {
          console.log("Datos escritos en 'users.json'.");
        }
      })
    };
